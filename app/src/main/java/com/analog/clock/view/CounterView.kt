package com.analog.clock.view

import android.content.Context
import android.graphics.Color
import android.os.Parcelable
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.analog.clock.R
import com.analog.clock.databinding.CounterViewBinding
import kotlin.math.absoluteValue
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize

class CounterView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRs: Int = 0
) :
    LinearLayout(context, attrs, defStyleAttr, defStyleRs) {

    private companion object {
        private const val MAX_VALUE = 99
        private const val MIN_VALUE = -99
    }

    private val greenColor by lazy(LazyThreadSafetyMode.NONE) { context.getColor(R.color.green) }
    private val redColor by lazy(LazyThreadSafetyMode.NONE) { context.getColor(R.color.red) }
    private val blackColor by lazy(LazyThreadSafetyMode.NONE) { Color.BLACK }

    private var counter = 0
    private var binding: CounterViewBinding

    init {
        gravity = Gravity.CENTER
        binding = CounterViewBinding
            .inflate(LayoutInflater.from(context), this)
        binding.likeButton.setOnClickListener { onUpBtnClick() }
        binding.dislikeButton.setOnClickListener { onDownBtnClick() }
        setCounter(counter)
        isSaveEnabled = true
        isSaveFromParentEnabled = true
    }

    private fun onDownBtnClick() {
        setCounter(counter - 1)
    }

    private fun onUpBtnClick() {
        setCounter(counter + 1)
    }

    fun setCounter(value: Int) {
        counter = value.coerceIn(MIN_VALUE, MAX_VALUE)
        val color = when {
            counter > 0 -> greenColor
            counter < 0 -> redColor
            else -> blackColor
        }
        with(binding) {
            counterTextView.setTextColor(color)
            counterTextView.text = counter.absoluteValue.toString()
        }
    }

    fun getCounter(): Int {
        return counter
    }

    override fun onSaveInstanceState(): Parcelable {
        val state = super.onSaveInstanceState()
        return SavedState(counter, state)
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        if (state !is SavedState) return super.onRestoreInstanceState(state)
        super.onRestoreInstanceState(state.superState)
        setCounter(state.counter)
    }

    @Parcelize
    class SavedState(
        val counter: Int,
        @IgnoredOnParcel val source: Parcelable? = null,
    ) : BaseSavedState(source)
}



